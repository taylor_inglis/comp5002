﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = String.Empty;
            Console.WriteLine("What is your name?");
            myName = (Console.ReadLine());
            Console.WriteLine($"Your name is {myName}");
        }
    }
}
