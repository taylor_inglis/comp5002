﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication8
{
    class Program
    {
        static void Main(string[] args)
        {
            var length = 0;
            var width = 0;
            Console.WriteLine("What is the length and width of your rectangle?");
            length = int.Parse(Console.ReadLine());
            width = int.Parse(Console.ReadLine());
            var perimeter = (2 * length) + (2 * width);
            Console.WriteLine($"The perimeter of your rectangle is {perimeter} metres");

        }
    }
}
