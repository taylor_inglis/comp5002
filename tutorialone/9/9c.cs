﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class Program
    {
        static void Main(string[] args)
        {
            var r = 0;
            Console.WriteLine("What is the radius of your circle?");
            r = int.Parse(Console.ReadLine());
            var cir = r * 3.14 * 2;
            Console.WriteLine($"The circumference of your circle is {cir} metres");
        }
    }
}
