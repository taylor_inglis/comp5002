﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication7
{
    class Program
    {
        static void Main(string[] args)
        {
            var length = 0;
            var width = 0;
            Console.WriteLine("What is the length and width of your rectangle");
            length = int.Parse(Console.ReadLine());
            width = int.Parse(Console.ReadLine());
            var area = length * width;
            Console.WriteLine($"The area of your rectangle is {area} square meters");
            

        }
    }
}
