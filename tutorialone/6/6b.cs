﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            const double mileKm = 1.609344;
            var km = 0;
            var miles = 0;
            Console.WriteLine("How many miles would you like converted to kilometers");
            miles = int.Parse(Console.ReadLine());
            var converted = mileKm * miles;
            Console.WriteLine($"{miles} km in miles is {converted}");
        }
    }
}
